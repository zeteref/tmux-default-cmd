#!/bin/bash

self="$(readlink -f "$0")"


get-tmux-option() {
	tmux show-option -gqv "$1"
}


new-window() {
    tmux showenv TMUX_CMD \
        | sed 's/^.*=//' \
    | xargs tmux new-window -c '#{pane_current_path}'
}


set-tmux-cmd() {
    tmux command-prompt -p "Set default new-window command:"  "setenv TMUX_CMD '%%'"
}


bind-user-keys() {
    local key="$(get-tmux-option '@default-cmd.bind.new-window')"
    if test -n "$key"; then
        tmux bind-key "$key" run "$self --new-window"
    fi

    local key="$(get-tmux-option '@default-cmd.bind.new-top-window')"
    if test -n "$key"; then
        tmux bind-key "$key" new-window -c "#{session_path}"
    fi

    local key="$(get-tmux-option '@default-cmd.bind.set-tmux-cmd')"
    if test -n "$key"; then
        tmux bind-key "$key" run "$self --set-tmux-cmd"
    fi
}


main() {
    bind-user-keys
}


case $1 in
    "")
        main
        ;;
    --new-window)
        new-window
        ;;
    --set-tmux-cmd)
        set-tmux-cmd
        ;;
    --help)
        help
        ;;
    *)
        echo "Invalid options $*" >&2
        exit 1
        ;;
esac
